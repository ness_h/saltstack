include:
  - lftp

install vim:
  pkg.installed:
    - name: {{ pillar['editor'] }}
    - name: {{ pillar['apache'] }}
remove vim:
  pkg.removed:
    - name: {{ pillar['editor'] }}
create my_new_directory:
 file.directory:
   - name: /opt/my_new_directory
   - user: root
   - group: root
   - mode: 755
Install mysql and make sure the mysql service is running:
  pkg.installed:
    - name: mysql
    - enable: True
  service.running:
    - name: mysql
Clone the SaltStack bootstrap script repo:
  pkg.installed: 
    - name: git # make sure git is installed first!
  git.latest:
    - name: https://github.com/saltstack/salt-bootstrap
    - rev: develop
    - target: /tmp/salt
user account for pete:
  user.present:
    - name: pete
    - shell: /bin/bash
    - home: /home/pete
    - groups:
      - sudo
myserver in hosts file:
  host.present:
    - name: myserver
    - ip: 192.168.0.42
restart vsftpd:
  module.run:
    - name: service.restart
    - m_name: vsftpd  # m_name gets passed to the execution module as "name"
deploy the http.conf file:
  file.managed:
    - name: /etc/http/conf/http.conf
    - source: salt://apache/http.conf #/srv/salt/apache/http.conf 
copy some files to the web server:
  file.recurse:
    - name: /var/www
    - source: salt://apache/www

    

