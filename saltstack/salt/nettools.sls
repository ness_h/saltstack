install_network_packages:
  pkg.installed:
    - pkgs:
      - rsync
      - lftp
      - curl
copy lftp config file:
  file.managed:
    - name: /etc/lftp.conf
    - text: set net:limit-rate 100000:500000